package projeto.janelas.modelo;

public class Compra {
	
	private String nome;
	private String idCompra;
	private String cpf;
	private String produto;
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getIdCompra() {
		return idCompra;
	}
	
	public void setIdCompra(String idCompra) {
		this.idCompra = idCompra;
	}
	
	public String getCpf() {
		return cpf;
	}
	
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	public String getProduto() {
		return produto;
	}
	
	public void setProduto(String produto) {
		this.produto = produto;
	}
	
}
	
