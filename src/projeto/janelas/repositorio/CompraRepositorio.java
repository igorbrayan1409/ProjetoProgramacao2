package projeto.janelas.repositorio;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CompraRepositorio {

	public void inserir(String cpf,String nome, String produto) throws Exception {
		Connection conn = Conexao.getConexao();
		PreparedStatement stmt = conn.prepareStatement("insert into compra (nome,cpf,produto) values(?,?,?)");
		stmt.setString(1, nome);
		stmt.setString(2, cpf);
		stmt.setString(3, produto);
		if (nome.equals("")) {
			throw new Exception("Nome n�o pode ser vazio !!!");
		} else {
			stmt.executeUpdate();
		}	
		stmt.close();
		conn.close();
	}
	
	public void apagar(String nome) throws SQLException {
		Connection conn = Conexao.getConexao();
		PreparedStatement stmt = conn.prepareStatement("delete from compra where nome=?");
		stmt.setString(1, nome);
		stmt.executeUpdate();
		stmt.close();
		conn.close();		
	}
	
	public void atualizar(String produto, String cpf) throws SQLException {
		//Atualiza o nome do produto pelo CPF;
		Connection conn = Conexao.getConexao();
		PreparedStatement stmt = conn.prepareStatement("update compra set produto=? where cpf=?");
		stmt.setString(1,produto);		
		stmt.setString(2,cpf);
		stmt.executeUpdate();
		stmt.close();
		conn.close();
	}
	
	public List<String> listarTodos(){
		List<String> nomePessoas = new ArrayList<String>();
		List<String> cpf = new ArrayList<String>();
		try {
			Connection conn = Conexao.getConexao();
			Statement stm = conn.createStatement();
			ResultSet rs = stm.executeQuery("select * from compra");
			while (rs.next()) {
				nomePessoas.add(rs.getString(2));
				cpf.add(rs.getString(3));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return nomePessoas;
	}
	
	//M�todo que lista todos os registros buscando cpf e nome
	public Map<String, String> listAll(){
		
		Map<String, String> mapaCliente = new HashMap<String, String>();
		try {
			Connection conn = Conexao.getConexao();
			Statement stm = conn.createStatement();
			ResultSet rs = stm.executeQuery("select * from compra");
			while (rs.next()) {
				mapaCliente.put(rs.getString(1), rs.getString(3));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return mapaCliente;
	}
	
	public String listar(String codigo) {
		String nomePessoa="";
		
		try {
			Connection conn = Conexao.getConexao();
			PreparedStatement stmt = conn.prepareStatement("select nome from compra where nome=?");	
			stmt.setInt(1, Integer.parseInt(codigo));
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				nomePessoa = rs.getString(1);
			}
			if (nomePessoa.equals("")){
				throw new Exception("O nome do cliente est� em branco!!");
			}
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			System.err.println("Mensagem: "+e.getMessage());
		}
		return nomePessoa;
	}
	
}
