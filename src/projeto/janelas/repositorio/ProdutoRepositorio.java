package projeto.janelas.repositorio;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProdutoRepositorio {

	public void inserir(String nome,String valor) throws Exception {
		Connection conn = Conexao.getConexao();
		PreparedStatement stmt = conn.prepareStatement("insert into produto (nome,valor) values(?,?)");
		stmt.setString(1, nome);
		//stmt.setInt(2, Integer.parseInt(valor));
		stmt.setString(2, valor);
		if (nome.equals("")) {
			throw new Exception("Nome do produto n�o pode ser vazio !!!");
		} else {
			stmt.executeUpdate();
		}	
		stmt.close();
		conn.close();
	}
	
	public void apagar(String nome) throws SQLException {
		Connection conn = Conexao.getConexao();
		PreparedStatement stmt = conn.prepareStatement("delete from produto where nome=?");
		stmt.setString(1,nome);
		stmt.executeUpdate();
		stmt.close();
		conn.close();		
	}
	
	public void atualizar(String nome, String valor) throws SQLException {
		Connection conn = Conexao.getConexao();
		//Atualiza o valor do produto;
		PreparedStatement stmt = conn.prepareStatement("update produto set valor=? where nome=?");
		stmt.setString(1,nome);		
		stmt.setString(2,valor);
		stmt.executeUpdate();
		stmt.close();
		conn.close();
	}
	
	public List<String> listarTodos(){
		List<String> nomeProduto = new ArrayList<String>();
		List<String> valor = new ArrayList<String>();
		try {
			Connection conn = Conexao.getConexao();
			Statement stm = conn.createStatement();
			ResultSet rs = stm.executeQuery("select * from produto");
			while (rs.next()) {
				nomeProduto.add(rs.getString(2));
				valor.add(rs.getString(3));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return nomeProduto;
	}
	
	//M�todo que lista todos os registros buscando nome e valor;
	public Map<String, String> listAll(){
		
		Map<String, String> mapaProduto = new HashMap<String, String>();
		try {
			Connection conn = Conexao.getConexao();
			Statement stm = conn.createStatement();
			ResultSet rs = stm.executeQuery("select * from produto");
			while (rs.next()) {
				mapaProduto.put(rs.getString(3), rs.getString(2));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return mapaProduto;
	}
	
	public String listar(String codigo) {
		String nomeProduto="";
		
		try {
			Connection conn = Conexao.getConexao();
			PreparedStatement stmt = conn.prepareStatement("select nome from produto where nome=?");	
			stmt.setInt(1, Integer.parseInt(codigo));
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				nomeProduto = rs.getString(1);
			}
			if (nomeProduto.equals("")){
				throw new Exception("O nome do produto est� em branco !!!");
			}
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			System.err.println("Mensagem: "+e.getMessage());
		}
		return nomeProduto;
	}
	
}
