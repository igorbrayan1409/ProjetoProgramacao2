package projeto.janelas.repositorio;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

class Conexao {

	public static Connection getConexao() throws SQLException {
		return DriverManager.getConnection("jdbc:mysql://localhost:3306/loja?useTimezone=true&serverTimezone=UTC","root","");
	}
	
}

