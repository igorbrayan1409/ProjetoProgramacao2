package projeto.janelas.visao;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import projeto.janelas.repositorio.CompraRepositorio;

public class JanelaCadastroCompra extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6832829989494438253L;
	private JButton inserirButton;
	private JButton apagarButton;
	private JButton atualizarButton;
	private JButton listarButton;
	
	private JLabel cpfLabel;
	private JTextField cpfTextField;
	
	private JLabel nomeLabel;
	private JTextField nomeTextField;
	
	private JLabel produtoLabel;
	private JTextField produtoTextField;
	
	private JPanel cadastroPanel;
	private JPanel botoesPanel;
	
	private Container container;
	
	//Conexao com o banco de dados;
	private CompraRepositorio pB = new CompraRepositorio();
	
	public JanelaCadastroCompra() {
		super("Cadastrar Cliente");
		
		nomeLabel = new JLabel("Nome");
		nomeTextField = new JTextField(30);
		
		cpfLabel = new JLabel("Cpf");
		cpfTextField = new JTextField(10);
		
		produtoLabel = new JLabel("Produto");
		produtoTextField = new JTextField(10);
		
		cadastroPanel = new JPanel(new GridLayout(4,1));
		cadastroPanel.add(nomeLabel);
		cadastroPanel.add(nomeTextField);
		cadastroPanel.add(cpfLabel);
		cadastroPanel.add(cpfTextField);
		cadastroPanel.add(produtoLabel);
		cadastroPanel.add(produtoTextField);
		
		inserirButton = new JButton("Inserir");
		inserirButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				try {
					pB.inserir(cpfTextField.getText(), nomeTextField.getText(), produtoTextField.getText());
					JOptionPane.showMessageDialog(null,"Registro inserido com sucesso !!!");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(null, "Voc� errou !!! "+e.getMessage());
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
				}
			}
			
		});
			
		apagarButton = new JButton("Apagar");
		apagarButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				try {
					pB.apagar(nomeTextField.getText());
					cpfTextField.setText("");
					nomeTextField.setText("");
					JOptionPane.showMessageDialog(null,"Registro exclu�do com sucesso!!");
				} catch (SQLException e) {
					JOptionPane.showMessageDialog(null, "Voc� errou !!!"+e.getMessage());
				}
			}
			
		});		
		atualizarButton = new JButton("Atualizar");
		atualizarButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				try {
					pB.atualizar(produtoTextField.getText(), cpfTextField.getText());
					JOptionPane.showMessageDialog(null,"Registro atualizado com sucesso!!");					
				} catch (SQLException e) {
					JOptionPane.showMessageDialog(null, "Voc� errou !!!"+e.getMessage());
				}
				
			}
			
		});		
		
		listarButton = new JButton("Listar");
		listarButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				JanelaListagemCompra jl=new JanelaListagemCompra();
			}
			
		});
		
		botoesPanel = new JPanel(new GridLayout(1,5));
		botoesPanel.setPreferredSize(new Dimension(100,50));
		botoesPanel.add(inserirButton);
		botoesPanel.add(apagarButton);
		botoesPanel.add(atualizarButton);
		botoesPanel.add(listarButton);
		
		container = getContentPane();
		container.setLayout(new BorderLayout());
		container.add(cadastroPanel, BorderLayout.CENTER);
		container.add(botoesPanel, BorderLayout.SOUTH);
		
		setSize(500,250);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		
	}
	
	public static void main(String[] args) {
		JanelaCadastroCompra jc = new JanelaCadastroCompra();
	}
	
}
