package projeto.janelas.visao;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.ScrollPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Map;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;

import projeto.janelas.repositorio.*;

public class JanelaListagemProduto extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4781913830124967421L;
	private JPanel listPanel;
	private JLabel cpf;
	private JPanel buttonPanel;
	private JButton buttonOk;
	private JList list;
	private ScrollPane scrollPane;
	
	public JanelaListagemProduto() {
		super("Listagem de produtos");
		
		DefaultListModel listModel= new DefaultListModel<String>();
		
		/*
		ClienteRepositorio pb = new ClienteRepositorio();
		for (String str: pb.listarTodos()) {
			listModel.addElement(str);
		}	
		*/
		
		ProdutoRepositorio pb = new ProdutoRepositorio();
		
		for (Map.Entry<String, String> str: pb.listAll().entrySet()) {
			listModel.addElement(str);
		}	
	
		
		
		list = new JList(listModel);
		scrollPane = new ScrollPane();
		scrollPane.add(list);
		
		listPanel=new JPanel(new GridLayout(1,1));
		listPanel.add(scrollPane);
		

		buttonOk = new JButton("OK");
		buttonOk.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				dispose();
			}
			
		});
		buttonPanel=new JPanel(new FlowLayout());
		buttonPanel.add(buttonOk);
		
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(listPanel,BorderLayout.CENTER);
		getContentPane().add(buttonPanel,BorderLayout.SOUTH);
		
		
		
		addWindowListener(new WindowAdapter() {
			
			@Override
			public void windowClosing(WindowEvent arg0) {
				// TODO Auto-generated method stub
				dispose();
			}
			
		});		
		
		setSize(500,300);
		setLocationRelativeTo(null);
		setResizable(false);
		setVisible(true);	
	}
	
}
